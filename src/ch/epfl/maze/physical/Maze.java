package ch.epfl.maze.physical;

import java.util.ArrayList;
import java.util.List;

/**
 * Maze in which an animal starts from a starting point and must find the exit.
 * Every animal added will have its position set to the starting point. The
 * animal is removed from the maze when it finds the exit.
 * 
 */

public final class Maze extends World {
	
	// Lists that will contain the animals which are in the maze (or copies):
	private List<Animal> animalsInMaze;
	private List<Animal> animalsCopy;
	
	/**
	 * Constructs a Maze with a labyrinth structure.
	 * 
	 * @param labyrinth
	 *            Structure of the labyrinth, an NxM array of tiles
	 */

	public Maze(int[][] labyrinth) {
		super(labyrinth);
		
		animalsInMaze = new ArrayList<Animal>();
		animalsCopy = new ArrayList<Animal>();
	}

	@Override
	public boolean isSolved() {
		
		return animalsInMaze.isEmpty();
	}

	@Override
	public List<Animal> getAnimals() {
		
		return new ArrayList<Animal>(animalsInMaze);
	}

	/**
	 * Determines if the maze contains an animal.
	 * 
	 * @param a
	 *            The animal in question
	 * @return <b>true</b> if the animal belongs to the world, <b>false</b>
	 *         otherwise.
	 */

	public boolean hasAnimal(Animal a) {
		
		return animalsInMaze.contains(a);
	}

	/**
	 * Adds an animal to the maze.
	 * 
	 * @param a
	 *            The animal to add
	 */

	public void addAnimal(Animal a) {
		
		// Sets the position of the animal a to the start point:
		a.setPosition(getStart());
		
		animalsInMaze.add(a);
		animalsCopy.add(a.copy());
	}

	/**
	 * Removes an animal from the maze.
	 * 
	 * @param a
	 *            The animal to remove
	 */

	public void removeAnimal(Animal a) {
		
		animalsInMaze.remove(a);
	}

	@Override
	public void reset() {
		
		animalsInMaze.clear();
		
		for (int i = 0; i < animalsCopy.size(); i++) {
			
			animalsInMaze.add(animalsCopy.get(i).copy());
		}
	}
}
