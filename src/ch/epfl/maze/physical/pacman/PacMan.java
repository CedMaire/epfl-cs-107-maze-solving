package ch.epfl.maze.physical.pacman;

import ch.epfl.maze.physical.Animal;
import ch.epfl.maze.physical.Daedalus;
import ch.epfl.maze.physical.Prey;
import ch.epfl.maze.util.Direction;
import ch.epfl.maze.util.Vector2D;

/**
 * Pac-Man character, from the famous game of the same name.
 * 
 */

public class PacMan extends Prey {

	public PacMan(Vector2D position) {
		super(position);
		
		// To avoid the random first step:
		setPreviousChoice(Direction.NONE);
	}

	@Override
	public Direction move(Direction[] choices, Daedalus daedalus) {
		
		// If there is no predator in the maze, then the PacMan moves randomly:
		if (daedalus.getPredators().isEmpty()) {
			
			return move(choices);
		}
		
		// If there is at least one predator in the game:
		else {
			
			// Take the choice that brings the PacMan the further from the nearest predator:
			findTheRightChoice(true, choices, nearestPredatorPosition(daedalus, choices));
			
			return getPreviousChoice();
		}
	}
	
	// Method that computes the position of the nearest predator:
	private final Vector2D nearestPredatorPosition(Daedalus daedalus, Direction[] choices){
		
		double maxDistance = Double.POSITIVE_INFINITY;
		double distLocal = 0;
		int optimalChoice = 0;
		
		for (int i = 0; i < daedalus.getPredators().size(); i++) {
			
			Vector2D distance = getPosition().sub(daedalus.getPredators().get(i).getPosition());
			distLocal = distance.dist();

			if (distLocal < maxDistance) {
				
				maxDistance = distLocal;
				optimalChoice = i;
			}
		}
		
		return daedalus.getPredators().get(optimalChoice).getPosition();
	}

	@Override
	public Animal copy() {
		
		return new PacMan(getPosition());
	}
}
