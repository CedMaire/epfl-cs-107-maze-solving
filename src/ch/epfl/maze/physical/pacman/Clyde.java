package ch.epfl.maze.physical.pacman;

import ch.epfl.maze.physical.Animal;
import ch.epfl.maze.physical.Daedalus;
import ch.epfl.maze.physical.Predator;
import ch.epfl.maze.util.Direction;
import ch.epfl.maze.util.Vector2D;

/**
 * Orange ghost from the Pac-Man game, alternates between direct chase if far
 * from its target and SCATTER if close.
 * 
 */

public class Clyde extends Predator {
	
	// Maximal distance to enter temporal scatter mode:
	private final int MAX_DISTANCE_TEMP_SCATTER = 4;
	
	/**
	 * Constructs a Clyde with a starting position.
	 * 
	 * @param position
	 *            Starting position of Clyde in the labyrinth
	 */

	public Clyde(Vector2D position) {
		super(position);
	}

	@Override
	public Direction move(Direction[] choices, Daedalus daedalus) {
		
		choosePreyIfNotChosen(daedalus);
		
		// If there is no prey:
		if (daedalus.getPreyChosen() == -1) {
			
			return move(choices);
		}
		
		// If a prey was chosen:
		else {
			
			return ghostChooseMove(choices, daedalus, ghostsTarget(daedalus));
		}
	}
	
	// Computing the position Clyde has to target:
	@Override
	protected Vector2D ghostsTarget(Daedalus daedalus) {
		
		// Position of the target:
		Vector2D targetPosition = super.ghostsTarget(daedalus);
		
		// Distance between Clyde and the target:
		double distance = getPosition().sub(targetPosition).dist();
		
		incrementModeCounter();
		
		// Find the mode in which we are:
		if (getChaseMode()) {
			
			if (distance < MAX_DISTANCE_TEMP_SCATTER) {
				
				chaseMode(targetPosition);
				targetPosition = getHomePosition();
			}
			else {
				
				targetPosition = chaseMode(targetPosition);
			}
		} 
		else {
			
			targetPosition = scatterMode();
		}
		
		return targetPosition;
	}

	@Override
	public Animal copy() {
		
		return new Clyde(getPosition());
	}
}
