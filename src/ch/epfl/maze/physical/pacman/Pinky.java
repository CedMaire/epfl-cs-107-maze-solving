package ch.epfl.maze.physical.pacman;

import ch.epfl.maze.physical.Animal;
import ch.epfl.maze.physical.Daedalus;
import ch.epfl.maze.physical.Predator;
import ch.epfl.maze.util.Direction;
import ch.epfl.maze.util.Vector2D;

/**
 * Pink ghost from the Pac-Man game, targets 4 squares in front of its target.
 * 
 */

public class Pinky extends Predator {
	
	// Variable containing the last choice of the prey:
	private Vector2D previousPositionPrey;
	
	/**
	 * Constructs a Pinky with a starting position.
	 * 
	 * @param position
	 *            Starting position of Pinky in the labyrinth
	 */

	public Pinky(Vector2D position) {
		super(position);
		
		previousPositionPrey = null;
	}

	@Override
	public Direction move(Direction[] choices, Daedalus daedalus) {
		
		choosePreyIfNotChosen(daedalus);
		
		// Random first step:
		if (previousPositionPrey == null) {
			
			nullPreviousChoiceMove(choices);
			previousPositionPrey = daedalus.getPreys().get(daedalus.getPreyChosen()).getPosition();
			
			return getPreviousChoice();
		}
		
		// If it's not the first step:
		else {
			
			// If there is no prey:
			if (daedalus.getPreyChosen() == -1) {
				
				return move(choices);
			}
			
			// If a prey was chosen:
			else {
				
				return ghostMoves(choices, daedalus, ghostsTarget(daedalus));
			}
		}	
	}
	
	// Computing the position Pinky has to target:
	@Override
	protected final Vector2D ghostsTarget(Daedalus daedalus) {
		
		Vector2D currentPositionPrey = super.ghostsTarget(daedalus);
		Vector2D targetPosition = currentPositionPrey.sub(previousPositionPrey);
		targetPosition = targetPosition.mul(4);
		targetPosition = currentPositionPrey.add(targetPosition);
		
		// Updating the previous position of the prey:
		previousPositionPrey = currentPositionPrey;
		
		return targetPosition;
	}

	@Override
	public Animal copy() {
		
		return new Pinky(getPosition());
	}
}
