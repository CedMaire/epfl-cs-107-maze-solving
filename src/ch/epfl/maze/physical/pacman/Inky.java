package ch.epfl.maze.physical.pacman;

import ch.epfl.maze.physical.Animal;
import ch.epfl.maze.physical.Daedalus;
import ch.epfl.maze.physical.Predator;
import ch.epfl.maze.util.Direction;
import ch.epfl.maze.util.Vector2D;

/**
 * Blue ghost from the Pac-Man game, targets the result of two times the vector
 * from Blinky to its target.
 * 
 */

public class Inky extends Predator {
	
	// The order of the ghosts shouldn't change during each game (because it was stated that there are no pac-gums), so it should be fix:
	private int blinkysIndex;
	
	/**
	 * Constructs a Inky with a starting position.
	 * 
	 * @param position
	 *            Starting position of Inky in the labyrinth
	 */

	public Inky(Vector2D position) {
		super(position);
		
		// To avoid the random first step:
		setPreviousChoice(Direction.NONE);
		
		// Convention to say the index was not chosen:
		blinkysIndex = -1;
	}

	@Override
	public Direction move(Direction[] choices, Daedalus daedalus) {
		
		choosePreyIfNotChosen(daedalus);
		
		// If there is no prey:
		if (daedalus.getPreyChosen() == -1) {
			
			return move(choices);
		}
		
		// If a prey was chosen:
		else {
			
			return ghostMoves(choices, daedalus, ghostsTarget(daedalus));
		}
	}
	
	// Computing the position Inky has to target:
	@Override
	protected Vector2D ghostsTarget(Daedalus daedalus) {
		
		return super.ghostsTarget(daedalus).mul(2).sub(returnBlinkysPosition(daedalus));
	}
	
	// A method that searches/returns the position of Blinky:
	private final Vector2D returnBlinkysPosition(Daedalus daedalus) {
		
		// Search the position of Blinky if not already done:
		if (blinkysIndex == -1) {
			
			for (int i = 0; i < daedalus.getPredators().size(); i++) {
				
				if (daedalus.getPredators().get(i).getClass() == (new Blinky(new Vector2D(0, 0))).getClass()) {
					
					blinkysIndex = i;
					break;
				}
			}
			
			// If no Blinky was found:
			if (blinkysIndex == -1) {
				
				return super.ghostsTarget(daedalus);
			}
		}
		
		return daedalus.getPredators().get(blinkysIndex).getPosition();
	}

	@Override
	public Animal copy() {
		
		return new Inky(getPosition());
	}
}
