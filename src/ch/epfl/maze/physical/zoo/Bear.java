package ch.epfl.maze.physical.zoo;

import java.util.List;

import ch.epfl.maze.physical.Animal;
import ch.epfl.maze.util.Direction;
import ch.epfl.maze.util.Vector2D;

/**
 * Bear A.I. that implements the Pledge Algorithm.
 * 
 */

public class Bear extends Animal {
	
	// A variable to store the favorite direction of the Bear:
	private Direction favoriteDirection;
	
	// Counts how many times the Bear turns right and left:
	private int counter;
	
	/**
	 * Constructs a bear with a starting position.
	 * 
	 * @param position
	 *            Starting position of the bear in the labyrinth
	 */

	public Bear(Vector2D position) {
		super(position);
		
		favoriteDirection = null;
		counter = 0;
	}

	/**
	 * Moves according to the <i>Pledge Algorithm</i> : the bear tries to move
	 * towards a favorite direction until it hits a wall. In this case, it will
	 * turn right, put its paw on the left wall, count the number of times it
	 * turns right, and subtract to this the number of times it turns left. It
	 * will repeat the procedure when the counter comes to zero, or until it
	 * leaves the maze.
	 */

	@Override
	public Direction move(Direction[] choices) {
		
		// If the Bear does not have a favorite direction, it choose one randomly:
		if (favoriteDirection == null) {
			
			nullPreviousChoiceMove(choices);
			favoriteDirection = getPreviousChoice();
		}
		
		// When the Bear already chose its favorite direction:
		else {
			
			// Creating a new list to have the choices in the relative direction of the Bear:
			List<Direction> relativeChoicesList = convertInRelativeChoicesAsList(choices);
			
			// When he did not encounter a wall, or passed the obstacle:
			if (counter == 0) {
				
				if (relativeChoicesList.contains(getPreviousChoice().relativeDirection(favoriteDirection))) {
					
					setPreviousChoice(favoriteDirection);
				}
				else if (relativeChoicesList.contains(Direction.RIGHT)) {
					
					setPreviousChoiceUnRelativeDirection(Direction.RIGHT);
					counter++;
				}
				else {
					
					setPreviousChoice(getPreviousChoice().reverse());
					counter += 2;
				}
			}
			
			// When the bear already turned:
			else {
				
				// If the Bear can go LEFT (in its direction), then it has to take it:
				if (relativeChoicesList.contains(Direction.LEFT)) {
					
					setPreviousChoiceUnRelativeDirection(Direction.LEFT);
					counter--;
				}
				
				// If not, if the Bear can go UP (in its direction), then it has to take it:
				else if (relativeChoicesList.contains(Direction.UP)) {
					
					setPreviousChoiceUnRelativeDirection(Direction.UP);
				}
				
				// If not, if the Bear can go RIGHT (in its direction), then it has to take it:
				else if (relativeChoicesList.contains(Direction.RIGHT)) {
					
					setPreviousChoiceUnRelativeDirection(Direction.RIGHT);
					counter++;
				}
				
				// If not, if the Bear can go DOWN (in its direction), then it has to take it:
				else if (relativeChoicesList.contains(Direction.DOWN)) {
					
					setPreviousChoiceUnRelativeDirection(Direction.DOWN);
					counter += 2;
				}
			}
		}
		
		return getPreviousChoice();
	}

	@Override
	public Animal copy() {
		
		return new Bear(getPosition());
	}
}
