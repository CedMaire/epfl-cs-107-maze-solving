package ch.epfl.maze.physical.zoo;

import java.util.ArrayList;
import java.util.List;

import ch.epfl.maze.physical.Animal;
import ch.epfl.maze.util.Direction;
import ch.epfl.maze.util.Vector2D;

/**
 * Panda A.I. that implements Trémeaux's Algorithm.
 * 
 */

public class Panda extends Animal {
	
	// Two ArrayLists that contains the colored tiles:
	private List<Vector2D> coloredOnce;
	private List<Vector2D> coloredTwice;
	
	/**
	 * Constructs a panda with a starting position.
	 * 
	 * @param position
	 *            Starting position of the panda in the labyrinth
	 */

	public Panda(Vector2D position) {
		super(position);
		
		coloredOnce = new ArrayList<Vector2D>();
		coloredTwice = new ArrayList<Vector2D>();
	}

	/**
	 * Moves according to <i>Trémeaux's Algorithm</i>: when the panda
	 * moves, it will mark the ground at most two times (with two different
	 * colors). It will prefer taking the least marked paths. Special cases
	 * have to be handled, especially when the panda is at an intersection.
	 */

	@Override
	public Direction move(Direction[] choices) {
		
		// If it is the first move:
		if (getPreviousChoice() == null) {
			
			if (choices.length == 1) {
				
				addPositionToColorTwo();
			}
			else {
				
				addPositionToColorOne();
			}
			
			nullPreviousChoiceMove(choices);
		}
		
		// If it is not the first move:
		else {
			
			// If there is only one choice, the panda has to take it:
			if (choices.length == 1) {
				
				// If its position is not colored in the second color:
				if (!(coloredTwice.contains(getPosition()))) {
					
					addPositionToColorTwo();
					
					// And if it it was colored in the fist color, it has to remove it:
					if (coloredOnce.contains(getPosition())) {
						
						removePositionFromColorOne();
					}
				}
				
				oneChoiceMove(choices);
			}
			
			// If there are exactly two choices:
			else if (choices.length == 2) {
				
				// If the position tile is colored in the first color, we have to remove it and add the second color:
				if (coloredOnce.contains(getPosition())) {
					
					swapColor();
				}
				
				// If not, and if it is not colored with the second color, then add the first color:
				else if (!(coloredTwice.contains(getPosition()))) {
					
					addPositionToColorOne();
				}
				
				twoChoiceMove(choices);
			}
			
			// And now if there are more than two choices:
			else {
				
				// These are three Lists to sort the choices in the different colors:
				List<Direction> choicesColorOne = new ArrayList<Direction>();
				List<Direction> choicesColorTwo = new ArrayList<Direction>();
				List<Direction> choicesColorVirgin = new ArrayList<Direction>();
				
				// A loop to sort all the different choices and add them to the corresponding list:
				for (int i = 0; i < choices.length; i++) {
					
					// If the choice has the first color:
					if (coloredOnce.contains(getPosition().addDirectionTo(choices[i]))) {
						
						choicesColorOne.add(choices[i]);
					}
					
					// If the choice has the second color:
					else if (coloredTwice.contains(getPosition().addDirectionTo(choices[i]))) {
						
						choicesColorTwo.add(choices[i]);
					}
					
					// If the choice has the no color at all:
					else {
						
						choicesColorVirgin.add(choices[i]);
					}
				}
				
				// If there are choices that have no color at all, the Panda has to take it:
				if (!(choicesColorVirgin.isEmpty())) {
					
					// And add its position to the tiles that have the first color (if it is not already):
					if (!(coloredOnce.contains(getPosition()))) {
						
						addPositionToColorOne();
					}
					
					randomChoice(choicesColorVirgin);
				}
				
				// If there are choices that have the first color:
				else if (!(choicesColorOne.isEmpty())) {
					
					// If there is only one tiles with the first color:
					if (choicesColorOne.size() == 1) {
						
						swapColor();
						setPreviousChoice(choicesColorOne.get(0));
					}
					
					// If there are only first color choices, the it has to go back:
					else if (choicesColorOne.size() == choices.length) {
						
						setPreviousChoice(getPreviousChoice().reverse());
					}
					
					// Otherwise it has to choose randomly:
					else {
						
						randomChoice(choicesColorOne);
					}
				}
				
				// Otherwise it has to choose randomly among the possibilities:
				else {
					
					randomChoice(choicesColorTwo);
				}
			}
		}
		
		return getPreviousChoice();
	}
	
	// A method to call the method intersectonChoiceMove with the right list of choices:
	private final void randomChoice(List<Direction> choices) {
		
		intersectionChoiceMove(choices.toArray(new Direction[choices.size()]));
	}
	
	// A method to add the position to the tiles that have the first color:
	private final void addPositionToColorOne() {
		
		coloredOnce.add(getPosition());
	}
	
	// A method to remove the position from the tiles that have the first color:
	private final void removePositionFromColorOne() {
		
		coloredOnce.remove(getPosition());
	}
	
	// A method to add the position to the tiles that have the second color:
	private final void addPositionToColorTwo() {
		
		coloredTwice.add(getPosition());
	}
	
	// A method to remove the first color and add the second:
	private final void swapColor() {
		
		coloredOnce.remove(getPosition());
		coloredTwice.add(getPosition());
	}

	@Override
	public Animal copy() {
		
		return new Panda(getPosition());
	}
}
