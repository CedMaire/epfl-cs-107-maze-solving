package ch.epfl.maze.physical.zoo;

import java.util.List;

import ch.epfl.maze.physical.Animal;
import ch.epfl.maze.util.Direction;
import ch.epfl.maze.util.Vector2D;

/**
 * Monkey A.I. that puts its hand on the left wall and follows it.
 * 
 */

public class Monkey extends Animal {

	/**
	 * Constructs a monkey with a starting position.
	 * 
	 * @param position
	 *            Starting position of the monkey in the labyrinth
	 */

	public Monkey(Vector2D position) {
		super(position);
	}

	/**
	 * Moves according to the relative left wall that the monkey has to follow.
	 */

	@Override
	public Direction move(Direction[] choices) {
		
		// If it is the first step of the monkey:
		if (getPreviousChoice() == null) {
			
			nullPreviousChoiceMove(choices);
		}
		
		// If it is not the first step of the monkey:
		else {
			
			// Creating a new list to have the choices in the relative direction of the Monkey:
			List<Direction> relativeChoicesList = convertInRelativeChoicesAsList(choices);
			
			// If the Monkey can go LEFT (in its direction), then it has to take it:
			if (relativeChoicesList.contains(Direction.LEFT)) {
				
				setPreviousChoiceUnRelativeDirection(Direction.LEFT);
			}
			
			// If not, if the Monkey can go UP (in its direction), then it has to take it:
			else if (relativeChoicesList.contains(Direction.UP)) {
				
				setPreviousChoiceUnRelativeDirection(Direction.UP);
			}
			
			// If not, if the Monkey can go RIGHT (in its direction), then it has to take it:
			else if (relativeChoicesList.contains(Direction.RIGHT)) {
				
				setPreviousChoiceUnRelativeDirection(Direction.RIGHT);
			}
			
			// If not, if the Monkey can go DOWN (in its direction), then it has to take it:
			else if (relativeChoicesList.contains(Direction.DOWN)) {
				
				setPreviousChoiceUnRelativeDirection(Direction.DOWN);
			}
		}
		
		return getPreviousChoice();
	}

	@Override
	public Animal copy() {
		
		return new Monkey(getPosition());
	}
}
