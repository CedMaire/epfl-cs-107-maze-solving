package ch.epfl.maze.physical.zoo;

import java.util.Arrays;
import java.util.List;

import ch.epfl.maze.physical.Animal;
import ch.epfl.maze.util.Direction;
import ch.epfl.maze.util.Vector2D;

/**
 * Space Invader A.I. that implements an algorithm of your choice.
 * <p>
 * Note that this class is considered as a <i>bonus</i>, meaning that you do not
 * have to implement it (see statement: section 6, Extensions libres).
 * <p>
 * If you consider implementing it, you will have bonus points on your grade if
 * it can exit a simply-connected maze, plus additional points if it is more
 * efficient than the animals you had to implement.
 * <p>
 * The way we measure efficiency is made by the test case {@code Competition}.
 * 
 * @see ch.epfl.maze.tests.Competition Competition 
 * 
 */

public class SpaceInvader extends Animal {
	
	// A variable to store its favorite direction:
	private Direction favoriteDirection;
	
	// A variable to know if the SpaceInvader turned right or left first:
	private boolean firstTurnRight;
	
	// A counter to know how many time the SpaceInvader turns right and left:
	private int counter;
	
	/**
	 * Constructs a space invader with a starting position.
	 * 
	 * @param position
	 *            Starting position of the mouse in the labyrinth
	 */

	public SpaceInvader(Vector2D position) {
		super(position);
		
		favoriteDirection = null;
		firstTurnRight = false;
		counter = 0;
	}

	/**
	 * Moves according to the Pledge algorithm (modified). The algorithm of the SpaceInvader is a modified version of the Pledge
	 * algorithm, so that if the SpaceInvader encounters a wall and can't turn right then it will try to turn left before
	 * trying to turn back.
	 */

	@Override
	public Direction move(Direction[] choices) {
		
		List<Direction> choicesList = Arrays.asList(choices);
		
		// If the SpaceInvader has no favorite direction (also its first step):
		if (favoriteDirection == null) {
			
			favoriteDirection = standardMove(choices);
			setPreviousChoice(favoriteDirection);
		}
		
		// If its not its first step but the choices contains its favorite direction and the counter is at 0, the SpaceInvader has to take it:
		else if (choicesList.contains(favoriteDirection) && counter == 0) {
			
			setPreviousChoice(favoriteDirection);
		}
		else {
			
			// Converting the choices into relative choices and store them into a list:
			List<Direction> relativeChoicesList = convertInRelativeChoicesAsList(choices);
			
			// First turn right if possible (counter = 0):
			if (relativeChoicesList.contains(Direction.RIGHT) && counter == 0) {
				
				setPreviousChoiceUnRelativeDirection(Direction.RIGHT);
				counter--;
				firstTurnRight = true;
			}
			
			// Or first turn left if right is not possible (counter = 0):
			else if (relativeChoicesList.contains(Direction.LEFT) && counter == 0) {
				
				setPreviousChoiceUnRelativeDirection(Direction.LEFT);
				counter++;
				firstTurnRight = false;
			}
			else {
				
				// If he first turned right:
				if (firstTurnRight) {
					
					spaceInvaderChoice(relativeChoicesList, Direction.LEFT, Direction.UP, Direction.RIGHT, Direction.DOWN, 1);
				}
				
				// If he first turned left:
				else {
					
					spaceInvaderChoice(relativeChoicesList, Direction.RIGHT, Direction.UP, Direction.LEFT, Direction.DOWN, -1);
				}
			}
		}
		
		return getPreviousChoice();
	}
	
	// A method that act similar to the Monkey but with some extras:
	private final void spaceInvaderChoice(List<Direction> relativeChoicesList, Direction first, Direction second, Direction third, Direction fourth, int plusOrMinus) {
		
		if (relativeChoicesList.contains(first)) {
			
			setPreviousChoiceUnRelativeDirection(first);
			counter += plusOrMinus;
		}
		else if (relativeChoicesList.contains(second)) {
			
			setPreviousChoiceUnRelativeDirection(second);
		}
		else if (relativeChoicesList.contains(third)) {
			
			setPreviousChoiceUnRelativeDirection(third);
			counter -= plusOrMinus;
		}
		else if (relativeChoicesList.contains(fourth)) {
			
			setPreviousChoiceUnRelativeDirection(fourth);
			counter -= 2*plusOrMinus;
		}
	}

	@Override
	public Animal copy() {
		
		return new SpaceInvader(getPosition());
	}
}
