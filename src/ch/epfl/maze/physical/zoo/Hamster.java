package ch.epfl.maze.physical.zoo;

import java.util.ArrayList;
import java.util.List;

import ch.epfl.maze.physical.Animal;
import ch.epfl.maze.util.Direction;
import ch.epfl.maze.util.Vector2D;

/**
 * Hamster A.I. that remembers the previous choice it has made and the dead ends
 * it has already met.
 * 
 */

public class Hamster extends Animal {
	
	// ArrayList containing all the tiles/possibilities already visited:
	private ArrayList<Vector2D> visitedDeadEnds;
	
	/**
	 * Constructs a hamster with a starting position.
	 * 
	 * @param position
	 *            Starting position of the hamster in the labyrinth
	 */

	public Hamster(Vector2D position) {
		super(position);
		
		visitedDeadEnds = new ArrayList<Vector2D>();
	}

	/**
	 * Moves without retracing directly its steps and by avoiding the dead-ends
	 * it learns during its journey.
	 */

	@Override
	public Direction move(Direction[] choices) {
		
		// Creating a list that will contain the choices:
		List<Direction> choicesList = new ArrayList<Direction>();
		
		// Adding all the choices that does not lead to a dead end:
		for (int i = 0; i < choices.length; i++) {
			
			if (!(visitedDeadEnds.contains(getPosition().addDirectionTo(choices[i])))) {
				
				choicesList.add(choices[i]);
			}
		}
		
		// If there are no choices left:
		if (choicesList.isEmpty()) {
			
			return Direction.NONE;
		}
		
		// Otherwise if it is its first step:
		else if (getPreviousChoice() == null) {
			
			nullPreviousChoiceMove(choices);
		}
		
		// If it is not the first step, but there is only one way that does not lead to a dead end:
		else if (choicesList.size() == 1) {
			
			// Then add the position to the dead ends list:
			visitedDeadEnds.add(getPosition());
			
			// And take the only choice left:
			setPreviousChoice(choicesList.get(0));
		}
		
		// And if there is more than one choice left:
		else {
			
			// Reconverts the list to a fixed array:
			Direction[] choicesListFixed = new Direction[choicesList.size()];
			choicesListFixed = choicesList.toArray(choicesListFixed);
			
			intersectionChoiceMove(choicesListFixed);
		}
		
		return getPreviousChoice();
	}

	@Override
	public Animal copy() {
		
		return new Hamster(getPosition());
	}
}
