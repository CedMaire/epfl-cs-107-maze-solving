package ch.epfl.maze.physical;

import ch.epfl.maze.util.Direction;
import ch.epfl.maze.util.Vector2D;

/**
 * Predator that kills a prey when they meet with each other in the labyrinth.
 * 
 */

abstract public class Predator extends Animal {

	/* constants relative to the Pac-Man game */
	public static final int SCATTER_DURATION = 14;
	public static final int CHASE_DURATION = 40;
	
	// Boolean to define if the predator is in chase mode or not:
	private boolean chaseMode;
	
	// Integer that counts the steps to know when to change the mode:
	private int modeCounter;
	
	// Constant to keep the home position of each predator:
	private final Vector2D homePosition;

	/**
	 * Constructs a predator with a specified position.
	 * 
	 * @param position
	 *            Position of the predator in the labyrinth
	 */

	public Predator(Vector2D position) {
		super(position);
		
		chaseMode = true;
		modeCounter = 0;
		homePosition = new Vector2D(position.getX(), position.getY());
	}
	
	protected final Vector2D getHomePosition() {
		
		return new Vector2D(homePosition.getX(), homePosition.getY());
	}
	
	protected final void choosePreyIfNotChosen(Daedalus daedalus) {
		
		if(daedalus.getPreyChosen() == -1) {
			
			daedalus.choosePrey();
		}		
	}
	
	protected final void incrementModeCounter() {
		
		modeCounter++;
	}
	
	protected final boolean getChaseMode() {
		
		return chaseMode;
	}
	
	// Computes the target of the predator:
	protected Vector2D ghostsTarget(Daedalus daedalus) {
		
		return daedalus.getPreys().get(daedalus.getPreyChosen()).getPosition();
	}

	/**
	 * Moves according to a <i>random walk</i>, used while not hunting in a
	 * {@code MazeSimulation}.
	 * 
	 */

	@Override
	public final Direction move(Direction[] choices) {
		
		return standardMove(choices);
	}

	/**
	 * Retrieves the next direction of the animal, by selecting one choice among
	 * the ones available from its position.
	 * <p>
	 * In this variation, the animal knows the world entirely. It can therefore
	 * use the position of other animals in the daedalus to hunt more
	 * effectively.
	 * 
	 * @param choices
	 *            The choices left to the animal at its current position (see
	 *            {@link ch.epfl.maze.physical.World#getChoices(Vector2D)
	 *            World.getChoices(Vector2D)})
	 * @param daedalus
	 *            The world in which the animal moves
	 * @return The next direction of the animal, chosen in {@code choices}
	 */

	abstract public Direction move(Direction[] choices, Daedalus daedalus);
	
	// General method to move a predator:
	protected final Direction ghostMoves(Direction[] choices, Daedalus daedalus, Vector2D position) {
		
		incrementModeCounter();
		
		return ghostChooseMove(choices, daedalus, changeMode(position));
	}
	
	// The standard version of the move method for predators:
	protected final Direction ghostChooseMove(Direction[] choices, Daedalus daedalus, Vector2D targetPosition) {
		
		if (choices.length == 1) {
			
			oneChoiceMove(choices);
		}
		else if (getPreviousChoice() == null) {
			
			nullPreviousChoiceMove(choices);
		}
		else if (choices.length == 2) {
			
			twoChoiceMove(choices);
		}
		else {
			
			// We want to computes the choices that brings us as near as possible:
			findTheRightChoice(false, choices, targetPosition);
		}
		
		return getPreviousChoice();
	}
	
	// Method to change the mode between chase and scatter:
	protected final Vector2D changeMode(Vector2D position) {
		
		Vector2D targetPosition = null;
		
		if (getChaseMode()) {
			
			targetPosition = chaseMode(position);
		}
		else {
			
			targetPosition = scatterMode();
		}
		
		return new Vector2D(targetPosition.getX(), targetPosition.getY());
	}
	
	// Method to switch from chase mode to scatter mode only if needed:
	protected final Vector2D chaseMode(Vector2D targetPosition) {
		
		if (modeCounter == CHASE_DURATION) {
			
			modeCounter = 0;
			chaseMode = false;
		}
		
		return new Vector2D(targetPosition.getX(), targetPosition.getY());
	}
	
	// Method to switch from scatter mode to chase mode only if needed:
	protected final Vector2D scatterMode() {
		
		if (modeCounter == SCATTER_DURATION) {
			
			modeCounter = 0;
			chaseMode = true;
		}
		
		return new Vector2D(homePosition.getX(), homePosition.getY());
	}
}
