package ch.epfl.maze.physical;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Daedalus in which predators hunt preys. Once a prey has been caught by a
 * predator, it will be removed from the daedalus.
 * 
 */

public final class Daedalus extends World {
	
	// Random initializer:
	private Random random = new Random();
	
	// Lists containing the predators and preys:
	private List<Predator> predatorsInDaedelus;
	private List<Predator> predatorsCopy;
	private List<Prey> preysInDaedelus;
	private List<Prey> preysCopy;
	
	// Refers to the index of the prey chosen:
	private int preyChosen;
	
	/**
	 * Constructs a Daedalus with a labyrinth structure
	 * 
	 * @param labyrinth
	 *            Structure of the labyrinth, an NxM array of tiles
	 */

	public Daedalus(int[][] labyrinth) {
		super(labyrinth);
		
		predatorsInDaedelus = new ArrayList<Predator>();
		preysInDaedelus = new ArrayList<Prey>();
		predatorsCopy = new ArrayList<Predator>();
		preysCopy = new ArrayList<Prey>();
		
		// Convention to state that the prey was not chosen:
		preyChosen = -1;
	}
	
	// Method to chose a common prey for all the predators:
	protected final void choosePrey() {
		
		if (!isSolved()) {
			
			preyChosen = random.nextInt(preysInDaedelus.size());
		}
		else {
			
			preyChosen = -1;
		}
	}
	
	// Method that returns the index of the prey chosen:
	public final int getPreyChosen() {
		
		return preyChosen;
	}

	@Override
	public boolean isSolved() {
		
		return preysInDaedelus.isEmpty();
	}

	/**
	 * Adds a predator to the daedalus.
	 * 
	 * @param p
	 *            The predator to add
	 */

	public void addPredator(Predator p) {
		
		predatorsInDaedelus.add(p);
		predatorsCopy.add((Predator) p.copy());
	}

	/**
	 * Adds a prey to the daedalus.
	 * 
	 * @param p
	 *            The prey to add
	 */

	public void addPrey(Prey p) {
		
		preysInDaedelus.add(p);
		preysCopy.add((Prey) p.copy());
	}

	/**
	 * Removes a predator from the daedalus.
	 * 
	 * @param p
	 *            The predator to remove
	 */

	public void removePredator(Predator p) {
		
		predatorsInDaedelus.remove(p);
	}

	/**
	 * Removes a prey from the daedalus.
	 * 
	 * @param p
	 *            The prey to remove
	 */

	public void removePrey(Prey p) {
		
		preysInDaedelus.remove(p);
		choosePrey();
	}

	@Override
	public List<Animal> getAnimals() {
		
		List<Animal> animalsInDaedalus = new ArrayList<Animal>();
		
		animalsInDaedalus.addAll(predatorsInDaedelus);
		animalsInDaedalus.addAll(preysInDaedelus);
		
		return animalsInDaedalus;
	}

	/**
	 * Returns a copy of the list of all current predators in the daedalus.
	 * 
	 * @return A list of all predators in the daedalus
	 */

	public List<Predator> getPredators() {
		
		return new ArrayList<Predator>(predatorsInDaedelus);
	}

	/**
	 * Returns a copy of the list of all current preys in the daedalus.
	 * 
	 * @return A list of all preys in the daedalus
	 */

	public List<Prey> getPreys() {
		
		return new ArrayList<Prey>(preysInDaedelus);
	}

	/**
	 * Determines if the daedalus contains a predator.
	 * 
	 * @param p
	 *            The predator in question
	 * @return <b>true</b> if the predator belongs to the world, <b>false</b>
	 *         otherwise.
	 */

	public boolean hasPredator(Predator p) {
		
		return predatorsInDaedelus.contains(p);
	}

	/**
	 * Determines if the daedalus contains a prey.
	 * 
	 * @param p
	 *            The prey in question
	 * @return <b>true</b> if the prey belongs to the world, <b>false</b>
	 *         otherwise.
	 */

	public boolean hasPrey(Prey p) {
		
		return preysInDaedelus.contains(p);
	}

	@Override
	public void reset() {
		
		predatorsInDaedelus.clear();
		preysInDaedelus.clear();
		
		for (int i = 0; i < predatorsCopy.size(); i++) {
			
			predatorsInDaedelus.add((Predator) predatorsCopy.get(i).copy());
		}
		
		for (int i = 0; i < preysCopy.size(); i++) {
			
			preysInDaedelus.add((Prey) preysCopy.get(i).copy());
		}
	}
}
