package ch.epfl.maze.physical;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ch.epfl.maze.util.Direction;
import ch.epfl.maze.util.Vector2D;

/**
 * World that is represented by a labyrinth of tiles in which an {@code Animal}
 * can move.
 * 
 */

public abstract class World {

	/* tiles constants */
	public static final int FREE = 0;
	public static final int WALL = 1;
	public static final int START = 2;
	public static final int EXIT = 3;
	public static final int NOTHING = -1;
	
	// Error string:
	private final String ERROR = "The labyrinth is not legit!";
	
	// Array representing the labyrinth:
	private final int[][] labyrinth;
	
	// Maximum of each:
	private final int MAX_START = 1;
	private final int MAX_EXIT = 1;
	
	// Constants representing the rows and columns:
	private final int labyrinthRows;
	private final int labyrinthColumns;
	
	// Variables containing the start and exit position:
	private Vector2D startPosition;
	private Vector2D exitPosition;

	/**
	 * Constructs a new world with a labyrinth. The labyrinth must be rectangle.
	 * 
	 * @param labyrinth
	 *            Structure of the labyrinth, an NxM array of tiles
	 */

	public World(int[][] labyrinth) {
		
		if (labyrinthIsRectangular(labyrinth)) {
			
			labyrinthRows = labyrinth.length;
			labyrinthColumns = labyrinth[0].length;
			
			startPosition = null;
			exitPosition = null;
			
			this.labyrinth = Arrays.copyOf(labyrinth, labyrinth.length);
			
			if (!checkStartAndExit()) {
				
				throw new IllegalArgumentException(ERROR);
			}
		}
		else {
			
			throw new IllegalArgumentException(ERROR);
		}
	}
	
	// Method to check if the labyrinth is rectangular:
	private final boolean labyrinthIsRectangular(int[][] labyrinth) {
		
		int numberOfRows = labyrinth[0].length;
		
		// Check if the labyrinth is rectangular:
		for (int i = 1; i < labyrinth.length; i++) {
			
			if (numberOfRows != labyrinth[i].length) {
				
				return false;
			}
		}
		
		return true;
	}
	
	// Method to check if the exit and start tiles are legit:
	private final boolean checkStartAndExit() {
		
		int counterStart = 0;
		int counterExit = 0;
		
		// Searching for the start and exit tiles and check if the tiles are legit:
		for (int j = 0; j < labyrinthColumns; j++) {
			for (int i = 0; i < labyrinthRows; i++) {
				
				if (getTile(j, i) != START && getTile(j, i) != EXIT && getTile(j, i) != FREE && getTile(j, i) != WALL && getTile(j, i) != NOTHING) {
					
					return false;
				}
				
				// Check/set the start position:
				else if (getTile(j, i) == START) {
					
					counterStart++;
					
					if (counterStart > MAX_START) {
						
						return false;
					}
					
					startPosition = new Vector2D(j, i);
				}
				
				// Check/set the exit position:
				else if (getTile(j, i) == EXIT) {
					
					counterExit++;
					
					if (counterExit > MAX_EXIT) {
						
						return false;
					}
					
					exitPosition = new Vector2D(j, i);
				}
			}
		}
		
		return true;
	}

	/**
	 * Determines whether the labyrinth has been solved by every animal.
	 * 
	 * @return <b>true</b> if no more moves can be made, <b>false</b> otherwise
	 */

	abstract public boolean isSolved();

	/**
	 * Resets the world as when it was instantiated.
	 */

	abstract public void reset();

	/**
	 * Returns a copy of the list of all current animals in the world.
	 * 
	 * @return A list of all animals in the world
	 */

	abstract public List<Animal> getAnimals();

	/**
	 * Checks in a safe way the tile number at position (x, y) in the labyrinth.
	 * 
	 * @param x
	 *            Horizontal coordinate
	 * @param y
	 *            Vertical coordinate
	 * @return The tile number at position (x, y), or the NONE tile if x or y is
	 *         incorrect.
	 */

	public final int getTile(int x, int y) {
		
		if ((x >= labyrinthColumns) || (y >= labyrinthRows) || (x < 0) || (y < 0)) {
			
			return NOTHING;
		}
		else {
			
			return labyrinth[y][x];
		}
	}

	/**
	 * Determines if coordinates are free to walk on.
	 * 
	 * @param x
	 *            Horizontal coordinate
	 * @param y
	 *            Vertical coordinate
	 * @return <b>true</b> if an animal can walk on tile, <b>false</b> otherwise
	 */

	public final boolean isFree(int x, int y) {
		
		if ((getTile(x, y) == WALL) || (getTile(x, y) == NOTHING)) {
			
			return false;
		}
		else {
			
			return true;
		}
	}

	/**
	 * Computes and returns the available choices for a position in the
	 * labyrinth. The result will be typically used by {@code Animal} in
	 * {@link ch.epfl.maze.physical.Animal#move(Direction[]) move(Direction[])}
	 * 
	 * @param position
	 *            A position in the maze
	 * @return An array of all available choices at a position
	 */

	public final Direction[] getChoices(Vector2D position) {
		
		// Stocks the position coordinates:
		final int X_COORDINATE = position.getX();
		final int Y_COORDINATE = position.getY();
		
		// Creates an ArrayList (we don't know the size needed) to stock the positions:
		List<Direction> choices = new ArrayList<Direction>();
		
		// Fills the ArrayList choices:
		if (isFree(X_COORDINATE, Y_COORDINATE - 1)) {
			
			choices.add(Direction.UP);
		}
		if (isFree(X_COORDINATE, Y_COORDINATE + 1)) {
			
			choices.add(Direction.DOWN);
		}
		if (isFree(X_COORDINATE + 1, Y_COORDINATE)) {
			
			choices.add(Direction.RIGHT);
		}
		if (isFree(X_COORDINATE - 1, Y_COORDINATE)) {
			
			choices.add(Direction.LEFT);
		}
		if (choices.isEmpty()) {
			
			choices.add(Direction.NONE);
		}
		
		// Converting the ArrayList to a fixed Array:
		Direction[] fixChoices = new Direction[choices.size()];
		fixChoices = choices.toArray(fixChoices);
		
		return fixChoices;
	}

	/**
	 * Returns horizontal length of labyrinth.
	 * 
	 * @return The horizontal length of the labyrinth
	 */

	public final int getWidth() {
		
		return labyrinthColumns;
	}

	/**
	 * Returns vertical length of labyrinth.
	 * 
	 * @return The vertical length of the labyrinth
	 */

	public final int getHeight() {
		
		return labyrinthRows;
	}

	/**
	 * Returns the entrance of the labyrinth at which animals should begin when
	 * added.
	 * 
	 * @return Start position of the labyrinth, null if none.
	 */

	public final Vector2D getStart() {
		
		return new Vector2D(startPosition.getX(), startPosition.getY());
	}

	/**
	 * Returns the exit of the labyrinth at which animals should be removed.
	 * 
	 * @return Exit position of the labyrinth, null if none.
	 */

	public final Vector2D getExit() {
		
		return new Vector2D(exitPosition.getX(), exitPosition.getY());
	}
}
