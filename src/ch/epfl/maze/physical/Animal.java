package ch.epfl.maze.physical;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import ch.epfl.maze.util.Direction;
import ch.epfl.maze.util.Vector2D;

/**
 * Animal inside a {@code World} that can move depending on the available
 * choices it has at its position.
 * 
 */

abstract public class Animal {
	
	public Random random = new Random();
	
	// This is the position of the animal:
	private Vector2D position;
	
	// A variable to stock the previous choice of the animal:
	private Direction previousChoice;
	
	/**
	 * Constructs an animal with a specified position.
	 * 
	 * @param position
	 *            Position of the animal in the labyrinth
	 */

	public Animal(Vector2D position) {
		
		setPosition(position);
		setPreviousChoice(null);
	}

	/**
	 * Retrieves the next direction of the animal, by selecting one choice among
	 * the ones available from its position.
	 * 
	 * @param choices
	 *            The choices left to the animal at its current position (see
	 *            {@link ch.epfl.maze.physical.World#getChoices(Vector2D)
	 *            World.getChoices(Vector2D)})
	 * @return The next direction of the animal, chosen in {@code choices}
	 */

	abstract public Direction move(Direction[] choices);
	
	// A general method that calls all the other general methods:
	protected final Direction standardMove(Direction[] choices) {
		
		// If the animal has only one choice, it'll take it:
		if (choices.length == 1) {
			
			oneChoiceMove(choices);
		}
		
		// If the animal had no previousChoice it'll take a random choice:
		else if (getPreviousChoice() == null) {
			
			nullPreviousChoiceMove(choices);
		}
		
		// If the animal has only two choices it will take the one that doesn't take it back:
		else if (choices.length == 2) {
			
			twoChoiceMove(choices);
		}
		
		// If the animal had a previousChoice and more than 2 choices it'll take a random choice that doesn't take it back:
		else {
			
			intersectionChoiceMove(choices);
		}
		
		return getPreviousChoice();
	}
	
	// A general method to move when the previousChoice variable is null:
	protected final void nullPreviousChoiceMove(Direction[] choices) {
		
		setPreviousChoice(choices[random.nextInt(choices.length)]);
	}
	
	// A general method to move when the animal has only one choice:
	protected final void oneChoiceMove(Direction[] choices) {
		
		setPreviousChoice(choices[0]);
	}
	
	// A general method to move if the animal has only two choices:
	protected final void twoChoiceMove(Direction[] choices) {
		
		if (choices[0] == getPreviousChoice().reverse()) {
			
			setPreviousChoice(choices[1]);
		}
		else {
			
			setPreviousChoice(choices[0]);
		}
	}
	
	// A general method to move when the animal has more than 2 choices:
	protected final void intersectionChoiceMove(Direction[] choices) {
		
		boolean goodChoice = false;
		Direction nextDirection = null;
		
		while (!goodChoice) {
			nextDirection = choices[random.nextInt(choices.length)];
			
			if (!(getPreviousChoice().isOpposite(nextDirection))) {
				goodChoice = true;
				setPreviousChoice(nextDirection);
			}
		}
	}
	
	// Computes the choice that brings as near as possible, or as far as possible:
	protected final void findTheRightChoice(boolean farAway, Direction[] choices, Vector2D targetPosition) {
		
		double maxDistance = 0;
		double minDistance = Double.POSITIVE_INFINITY;
		double distLocal = 0;
		int optimalChoice = 0;
		
		for (int i = 0; i < choices.length; i++) {
			if (!(getPreviousChoice().isOpposite(choices[i]))) {
				
				Vector2D distance = getPosition().addDirectionTo(choices[i]).sub(targetPosition);
				distLocal = distance.dist();
				
				if (farAway) {
					if (distLocal > maxDistance) {
						
						maxDistance = distLocal;
						optimalChoice = i;
					}
				}
				else {
					if (distLocal < minDistance) {
						
						minDistance = distLocal;
						optimalChoice = i;
					}
				}
			}
		}
		
		setPreviousChoice(choices[optimalChoice]);
	}
	
	/**
	 * Updates the animal position with a direction.
	 * <p>
	 * <b>Note</b> : Do not call this function in {@code move(Direction[]
	 * choices)} !
	 * 
	 * @param dir
	 *            Direction that the animal has taken
	 */

	public final void update(Direction dir) {
		
		setPosition(getPosition().add(dir.toVector()));
	}

	/**
	 * Sets new position for Animal.
	 * <p>
	 * <b>Note</b> : Do not call this function in {@code move(Direction[]
	 * choices)} !
	 * 
	 * @param position
	 */

	public final void setPosition(Vector2D position) {
		
		this.position = new Vector2D(position.getX(), position.getY());
	}

	/**
	 * Returns position vector of animal.
	 * 
	 * @return Current position of animal.
	 */

	public final Vector2D getPosition() {
		
		return new Vector2D(position.getX(), position.getY());
	}
	
	// Method that sets the previous choice to the new one:
	protected final void setPreviousChoice(Direction currentChoice) {
		
		previousChoice = currentChoice;
	}
	
	// Method that returns the previous choice of an animal:
	protected final Direction getPreviousChoice() {
		
		return previousChoice;
	}
	
	// A method that sets the previous choice to the unrelative direction from the parameter:
	protected final void setPreviousChoiceUnRelativeDirection(Direction relativeDirection) {
		
		setPreviousChoice(getPreviousChoice().unRelativeDirection(relativeDirection));
	}
	
	// A method that will return a list containing the choices in relative directions:
	public final List<Direction> convertInRelativeChoicesAsList(Direction[] choices) {
		
		Direction[] relativeChoicesArray = getPreviousChoice().relativeDirections(choices);
		List<Direction> relativeChoicesList = Arrays.asList(relativeChoicesArray);
		
		return relativeChoicesList;
	}

	abstract public Animal copy();
}
